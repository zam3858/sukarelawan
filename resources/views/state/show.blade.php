@extends('layouts.app')

@section('content')

<ul>
    <li> {{ __('Code') }} : {{ $state->code }} </li>
    <li> {{ __('Name') }} : {{ $state->name }} </li>
</ul>

@endsection