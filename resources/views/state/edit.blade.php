@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('states.update', $state->id) }}">
    @csrf
    @method('PUT')
  
  <div class="form-group">
    <label for="code">{{ __("Code") }}</label>
    <input type="text" 
        class="form-control" 
        id="code" 
        aria-describedby="code"
        name="code"
        value="{{ old('code') ?? $state->code }}"
    >
  </div>

  <div class="form-group">
  <label for="code">{{ __("Name") }}</label>
    <input type="text" 
        class="form-control" 
        id="name" 
        aria-describedby="name"
        name="name"
        value="{{ old('name') ?? $state->name }}"
        required
    >
  </div>

  <div class="form-group">
  <button class="btn btn-primary">{{ __("Submit") }} </button>
  </div>
</form>

@endsection