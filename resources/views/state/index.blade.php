@extends('layouts.app')

@section('content')

<table class="table table-striped table-dark">
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th></th>
    </tr>

    @foreach($states as $state)

    <tr>
        <td>{{ $state->code }}</td>
        <td>{{ $state->name }}</td>
        <td>
        <a class="btn btn-primary" href="{{ route('states.show', $state->id) }}"> {{ __('View') }} </a>
        <a class="btn btn-primary" href="{{ route('states.edit', $state->id) }}"> {{ __('Edit') }} </a>
        <a class="btn btn-warning" href="#" onclick="$('#del_{{ $state->id }}' ).submit()" > 
            {{ __('Delete') }} 
        </a>

        <form method="POST" id="del_{{ $state->id }}" 
            action="{{ route('states.destroy', $state->id) }}">
            @csrf 
            @method('DELETE') 
        </form>
        </td>
    </tr>

    @endforeach

</table>

@endsection

@section('script')

    <script>
        console.log("Hello world");
    </script>

@endsection