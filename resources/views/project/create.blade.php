@extends('layouts.app')

@section('content')

<form method="POST" action="{{ route('projects.store') }}">
    @csrf
  <div class="form-group">
    <label for="name">{{ __("Name") }}</label>
    <input type="text" 
        class="form-control" 
        id="name" 
        aria-describedby="name"
        name="name"
        value="{{ old('name') }}"
    >
    @error('name')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
    
  <div class="form-group">
    <label for="name">{{ __("Detail") }}</label>
    <input type="text" 
        class="form-control" 
        id="detail" 
        aria-describedby="detail"
        name="detail"
        value="{{ old('detail') }}"
    >
    @error('detail')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>

  <div class="form-group">
  <label for="user_id">{{ __("User") }}</label>
    <input type="text" 
        class="form-control" 
        id="user_id" 
        aria-describedby="user_id"
        name="user_id"
        value="{{ old('user_id') }}"    
    >
    @error('user_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>


  <div class="form-group">
  <button class="btn btn-primary">{{ __("Submit") }} </button>
  </div>
</form>

@endsection