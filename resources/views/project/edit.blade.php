@extends('layouts.app')

@section('content')

<form method="POST" 
  action="{{ route('projects.update', $project->id) }}"
  enctype="multipart/form-data"  
>
    @csrf
    @method('PUT')
  
  <div class="form-group">
    <label for="proj_doc">{{ __("Dokumen Projek") }}</label>
    <input type="file" 
        class="form-control-file" 
        id="proj_doc" 
        aria-describedby="proj_doc"
        name="proj_doc"
        value="{{ old('proj_doc', $project->proj_doc)  }}"
    >

    @if($project->proj_doc)
        <a href="{{ url('storage/' . $project->proj_doc) }}" 
            target="_BLANK" >Download</a>

        <input type='checkbox' name="delete_proj_doc" value="1" /> Check to delete   
    @endif

    @error('proj_doc')
      {{ $message }}
    @enderror
  </div>

  <div class="form-group">
    <label for="name">{{ __("Name") }}</label>
    <input type="text" 
        class="form-control" 
        id="name" 
        aria-describedby="name"
        name="name"
        value="{{ old('name', $project->name)  }}"
    >
    @error('name')
      {{ $message }}
    @enderror
  </div>

  <div class="form-group">
    <label for="detail">{{ __("Detail") }}</label>
    <input type="text" 
        class="form-control" 
        id="detail" 
        aria-describedby="detail"
        name="detail"
        value="{{ old('detail', $project->detail)  }}"
    >
    @error('detail')
      {{ $message }}
    @enderror
  </div>

  <div class="form-group">
    <label for="user_id">{{ __("User") }}</label>
    <input type="text" 
        class="form-control" 
        id="user_id" 
        aria-describedby="user_id"
        name="user_id"
        value="{{ old('user_id', $project->user_id)  }}"
    >
    @error('user_id')
      {{ $message }}
    @enderror
  </div>

  <div class="form-group">
  <button class="btn btn-primary">{{ __("Submit") }} </button>
  </div>
</form>

@endsection