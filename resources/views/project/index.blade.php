@extends('layouts.app')

@section('content')

<div class="rows">
<form action="">
    <div class="col-md-6">
        <input type="text" name="search" value="{{ old('search') }}" class="form-control">
    </div>
    <div class="col-md-6">
        <input type="submit" value="Cari" class="btn btn-primary">
    </div>
</form>
</div>

<table class="table table-striped table-dark">
    <tr>
        <th>City</th>
        <th>Name</th>
        <th>Detail</th>
        <th>File</th>
        <th>User</th>
        <th></th>
    </tr>

    @foreach($cities as $city)
        @foreach($city->projects as $project)

        <tr>
            <td>{{ $city->name }}</td>
            <td>{{ $project->name }}</td>
            <td>{{ $project->detail }}</td>
            <td>
                @if($project->proj_doc)
                    <a href="{{ url('storage/' . $project->proj_doc) }}" 
                        target="_BLANK" >Download</a>
                @endif
            </td>
            <td>{{ $project->owner->name ?? '' }}</td>
            <td>
            <a class="btn btn-primary" href="{{ route('projects.show', $project->id) }}"> {{ __('View') }} </a>
            <a class="btn btn-primary" href="{{ route('projects.edit', $project->id) }}"> {{ __('Edit') }} </a>
            <a class="btn btn-warning" href="#" onclick="$('#del_{{ $project->id }}' ).submit()" > 
                {{ __('Delete') }} 
            </a>

            <form method="POST" id="del_{{ $project->id }}" 
                action="{{ route('projects.destroy', $project->id) }}">
                @csrf 
                @method('DELETE') 
            </form>
            </td>
        </tr>

        @endforeach
    @endforeach

</table>

@endsection

@section('script')

    <script>
        console.log("Hello world");
    </script>

@endsection