@extends('layouts.app')

@section('content')
<div class="card">
    <ul>
        <li> {{ __('Name') }} : {{ $project->name }} </li>
        <li> {{ __('Detail') }} : {{ $project->detail }} </li>
        <li> {{ __('User') }} : {{ $project->user_id }} </li>
    </ul>
</div>

@endsection