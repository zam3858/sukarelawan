<?php

namespace App\Http\Controllers;

use App\Model\Project;
use App\Model\City;
use Illuminate\Http\Request;
use App\Http\Requests\Project\Store;
use App\Http\Requests\Project\Update;
use Illuminate\Support\Facades\Storage;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        // with() <-- eager loading. 
        //            code akan dapatkan terus data berkait.
        // 
        // takde with() <-- lazy loading.
        //                  data dia query semasa digunakan   

        $search = request()->search;

        $project_city_ids = [];
        if($search) {
            $project_city_ids = Project::where('name', 'like', '%' . $search . '%')
                                    ->orWhere('detail', 'like', '%' . $search . '%')
                                    ->get()
                                    ->pluck('city_id')
                                    ->toArray()
                                    ;
        }
        

        $cities = City::with('projects')
                        ->when($search, function($query) use ($search) { 
                            $query->where('name', 'like', '%' . $search . '%');
                        })
                        // ->when($project_city_ids, function($query) use ($project_city_ids) {
                        //     $query->has('projects', function($query) use ($project_city_ids) {                                
                        //             $query->whereIn('city_id', $project_city_ids);
                        //     });
                        
                        // })
                        ->orderBy('name')
                        ->get();

        //$projects = Project::get();
        
        return view('project.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::get();
        $user = User::get();
        return view('project.create', compact('cities','user') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {

        $project = new Project();

        $project->name = request()->name;
        $project->detail = $request->detail;
        $project->user_id = \Auth::user()->id; //id of current logged in user

        $project->save();

        return redirect( route('projects.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);
        return view('project.show', ['project' => $project ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('project.edit', ['project' => $project ] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Project $project)
    {
        //use Illuminate\Support\Facades\Storage;

        if( request()->has('delete_proj_doc') ) {
            //delete bila ada orang tick checkbox delete file
            Storage::disk('public')->delete( $project->proj_doc);
            $project->proj_doc = null;
        }

        // Request::hasFile() <-- check if ada upload file
        if( request()->hasFile('proj_doc') ) {
            //if ada update file, delete file lama
            Storage::disk('public')->delete( $project->proj_doc);

            $path = request()->file('proj_doc')
                        ->store('projects','public');
            $project->proj_doc = $path;
        }

        $project->name = request()->name;
        $project->detail = $request->detail;
        $project->user_id = $request->user_id;

        $project->save();

        \Session::flash('alert', [
                'type' => 'success',
                'message' => 'Rekod berjaya dikemaskini',
                ] );

        return redirect( route('projects.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {

        $project->delete();
        return redirect( route('projects.index') );

    }

    public function query1() {
        // SELECT * 
        // FROM projects
        // WHERE 1=1
        // AND id > 2
        // AND id < 5
        
        return Project::where('id', '>', 2)
                        ->where('id', '<', 5)
                        ->get();
    }

    public function query2() {
        /*
         SELECT projects.*, users.* 
         FROM projects
         LEFT JOIN users ON ( users.id = projects.user_id 
                            AND  users.name = 'zamrul')
         WHERE 1=1 
         AND WHERE users.id NOT NULL
         AND WHERE users.name = 'zamrul'
        */

        $city_ids = City::where('name','Cyberjaya')
        ->get()
        ->pluck('id')
        ->toArray();

        $projects = Project::with([ 
                            'bandar',
                             'owner' => function($query) {
                                $query->where('name', 'zamrul');
                             }, 
                             'owner.projects',
                             ])
                        ->whereHas('owner', function($query) {
                            $query->where('name','admin');
                        })
                        ->whereIn('city_id',$city_ids )
                        // ->whereHas('bandar', function($query) {
                        //     $query->where('name','Cyberjaya');
                        // }) // tak akan jalan sebab dalam DB lain 
                        ->get();

        return $projects;

    }

    public function query3() {
        // SELECT * 
        // FROM projects
        // WHERE 1=1
        // AND ( id > 2 OR id < 5 )
        // OR name LIKE '%ada%'
        
        return Project::where(function($query) {
                           $query->where('id', '>', 2)
                            ->orWhere('id', '<', 5);
                        })
                        ->where('name', 'like', '%ada%' )
                        ->get();
    }

    public function query4() {
        // SELECT count('id') as project_count, city_id
        // FROM projects
        // GROUP BY city_id

        return Project::select(\DB::raw("count('id') as project_count, city_id"))
                        ->groupBy('city_id')
                        ->with('bandar')
                        ->get();
    }

    //web.php
    // http://sukarelawan.test/query2
    // Route::get('query1', 'ProjectController@query1');
    // Route::get('query2', 'ProjectController@query2');
    // Route::get('query3', 'ProjectController@query3');
}
