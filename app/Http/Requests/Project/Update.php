<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // http://sukarelawan.test/projects/2
        $id = request()->segment(2);

        return [
            'name' => [ 'required',
                        'min:3', 
                        Rule::unique('projects','name')
                        ->ignore($id)
        
                      ],
            'detail' => ['required', 'alpha' ],
            'user_id' => ['required', 'integer'],
            'proj_doc' =>  'file|max:10' //~2mb limit
        ];
    }

    public function messages() {
        return [
            'name.required' => "nama projek diperlukan",
            'name.min' => "nama projek perlu lebih besar dari 3 aksara",
            'name.unique' => "nama projek sudah digunakan",

            'detail.required' => "keterangan projek diperlukan",

            'user_id.required' => "maklumat owners projek diperlukan",
            'user_id.integer' => "maklumat pengguna perlu dalam bentuk nombor"
        ];
    }
}