<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';
    protected $connection='mydb2';

    public function projects() {
        return $this->hasMany('\App\Model\Project','city_id');
    }
}
