<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

// handle satu table dalam database
// 
class Project extends Model
{
    protected $table="projects";
    protected $primaryKey = 'id';
    protected $connection = 'mysql';
    //protected $keyType = 'string';

    public function bandar() {
        return $this->belongsTo('App\Model\City', 'city_id');
    }

    public function owner() {
        return $this->belongsTo('App\User', 'user_id');
    }
    
}

