<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group( function() {
    //semua dalam ni wajib login

    Route::resource('projects', 'ProjectController');

    Route::resource('states','StateController');

});

Route::get('query1', 'ProjectController@query1');
Route::get('query2', 'ProjectController@query2');
Route::get('query3', 'ProjectController@query3');
Route::get('query4', 'ProjectController@query4');
   